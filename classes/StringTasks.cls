public with sharing class StringTasks {

    /**
 * Returns the result of concatenation of two strings.
 *
 * @param {string} value1
 * @param {string} value2
 * @return {string}
 *
 * @example
 *   'aa', 'bb' => 'aabb'
 *   'aa',''    => 'aa'
 *   '',  'bb'  => 'bb'
 */
    public static String concatenateStrings(String value1, String value2) {
        String result = value1+value2;
        return result;
    }

    /**
 * Returns the length of given string.
 *
 * @param {string} value
 * @return {number}
 *
 * @example
 *   'aaaaa' => 5
 *   'b'     => 1
 *   ''      => 0
 */
    public static Integer getStringLength(String value) {
        Integer result = value.length();
        return result;
    }

    /**
 * Returns the result of string template and given parameters firstName and lastName.
 * Please do not use concatenation, use template string :
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/template_strings
 *
 * @param {string} firstName
 * @param {string} lastName
 * @return {string}
 *
 * @example
 *   'John','Doe'      => 'Hello, John Doe!'
 *   'Chuck','Norris'  => 'Hello, Chuck Norris!'
 */
    public static String getStringFromTemplate(String firstName, String lastName) {
        String template = 'Hello, {0} {1}!';
        String result = String.format(template, new List<String> { firstName, lastName });
        System.debug(result);
        return result;
    }
    /**
 * Extracts a name from template string 'Hello, First_Name Last_Name!'.
 *
 * @param {string} value
 * @return {string}
 *
 * @example
 *   'Hello, John Doe!' => 'John Doe'
 *   'Hello, Chuck Norris!' => 'Chuck Norris'
 */
    // при выполнении indexOf(); 
    public static String extractNameFromTemplate(String value) {
        String result = value.replace('Hello, ', '' );
        String result2 = result.replace('!', '' );
        String finish = result2;
        System.debug(finish);
        return finish;
    }

    /**
 * Returns a first char of the given string.
 *
 * @param {string} value
 * @return {string}
 *
 * @example
 *   'John Doe'  => 'J'
 *   'cat'       => 'c'
 */
    public static String getFirstChar(String value) {
        List<String> listStrings = new List<String>();
        listStrings = value.split('');
        System.debug(listStrings);
        return listStrings[0];
    }

    /**
 * Removes a leading and trailing whitespace characters from string.
 *
 * @param {string} value
 * @return {string}
 *
 * @example
 *   '  Abracadabra'    => 'Abracadabra'
 *   'cat'              => 'cat'
 *   '\tHello, World! ' => 'Hello, World!'
 */
    public static String removeLeadingAndTrailingWhitespaces(String value) {
        //s1.normalizeSpace()
        String result = value.normalizeSpace();
        System.debug(result);
        return result;
    }

    /**
 * Returns a string that repeated the specified number of times.
 *
 * @param {string} value
 * @param {string} count
 * @return {string}
 *
 * @example
 *   'A', 5  => 'AAAAA'
 *   'cat', 3 => 'catcatcat'
 */
    public static String repeatString(String value, Integer count) {
        String result='';
        for (Integer i=0; i<count; i++) {
            result = result + value;
        }
        return result;
    }

    /**
 * Remove the first occurrence of string inside another string
 *
 * @param {string} str
 * @param {string} value
 * @return {string}
 *
 * @example
 *   'To be or not to be', 'not'  => 'To be or to be'
 *   'I like legends', 'end' => 'I like legs',
 *   'ABABAB','BA' => 'ABAB'
 */
    public static String removeFirstOccurrences(String str, String value) {
        Integer result = str.indexOf(value);
        String resultString = str.replaceFirst(value, '');
        System.debug(resultString);
        return resultString;
    }

    /**
 * Remove the first and last angle brackets from tag string
 *
 * @param {string} str
 * @return {string}
 *
 * @example
 *   '<div>' => 'div'
 *   '<span>' => 'span'
 *   '<a>' => 'a'
 */
    public static String unbracketTag(String str) {
        String endString = str.removeEnd('>');
        String startString = endString.removeStart('<');
        return startString;
    }

    /**
 * Converts all characters of the specified string into the upper case
 *
 * @param {string} str
 * @return {string}
 *
 * @example
 *   'Thunderstruck' => 'THUNDERSTRUCK'
 *  'abcdefghijklmnopqrstuvwxyz' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
 */
    public static String convertToUpperCase(String str) {
        String result = str.toUpperCase();
        return result;
    }

    /**
 * Extracts e-mails from single string with e-mails list delimeted by semicolons
 *
 * @param {string} str
 * @return {array}
 *
 * @example
 *   'angus.young@gmail.com;brian.johnson@hotmail.com;bon.scott@yahoo.com' => ['angus.young@gmail.com', 'brian.johnson@hotmail.com', 'bon.scott@yahoo.com']
 *   'info@gmail.com' => ['info@gmail.com']
 */
    public static List<String> extractEmails(String str) {
        List<String> resultList = new List<String>();
        resultList = str.split(';');
        return resultList;
    }

    /**
 * Returns the string representation of rectangle with specified width and height
 * using pseudograhic chars
 *
 * @param {number} width
 * @param {number} height
 * @return {string}
 *
 * @example
 *
 *            '┌────┐\n'+
 *  (6,4) =>  '│    │\n'+
 *            '│    │\n'+
 *            '└────┘\n'
 *
 *  (2,2) =>  '┌┐\n'+
 *            '└┘\n'
 *
 *             '┌──────────┐\n'+
 *  (12,3) =>  '│          │\n'+
 *             '└──────────┘\n'
 *
 */
    public static String getRectangleString(Integer width, Integer height) {
        String result = '';
        result ='┌'; // ┏ г ┌ ┍ ┌
        String bottom = '└'; //└
        String medium = '';
        for (Integer i=0; i<width-2; i++) {
             medium += ' ';
        }
        for (Integer i=0; i<width-2; i++) {
            result += '─'; //╼ - ─
            bottom += '─';
        }
        result += '┐' + '\n'; // ┓ ┒ ┐ ┐
        bottom += '┘';  //┘ ┙ ┘
        for (Integer i=0; i<height-2; i++) { //┗  ┚
            result += '│'; // │
            result += medium;
            result += '│' + '\n'; //│
        }
        result += bottom + '\n';
        System.debug(result);
        return result;
    }

    /**
 * Returns playid card id.
 *
 * Playing cards inittial deck inclides the cards in the following order:
 *
 *  'A♣','2♣','3♣','4♣','5♣','6♣','7♣','8♣','9♣','10♣','J♣','Q♣','K♣',
 *  'A♦','2♦','3♦','4♦','5♦','6♦','7♦','8♦','9♦','10♦','J♦','Q♦','K♦',
 *  'A♥','2♥','3♥','4♥','5♥','6♥','7♥','8♥','9♥','10♥','J♥','Q♥','K♥',
 *  'A♠','2♠','3♠','4♠','5♠','6♠','7♠','8♠','9♠','10♠','J♠','Q♠','K♠'
 *
 * (see https://en.wikipedia.org/wiki/Standard_52-card_deck)
 * Function returns the zero-based index of specified card in the initial deck above.
 *
 * @param {string} value
 * @return {number}
 *
 * @example
 *   'A♣' => 0
 *   '2♣' => 1
 *   '3♣' => 2
 *     ...
 *   'Q♠' => 50
 *   'K♠' => 51
 */
    public static Integer getCardId(String value) {
        String incomingString = value;
        System.debug('in ' + incomingString);

        List<String> incomingStrings = value.split('');
        System.debug('size = ' + incomingStrings.size());
        String person = '';
        String mast = '';
        Integer ten = 0;
        Integer result = 0;

        if (incomingStrings.size()==3) {
            if (incomingStrings.contains('♣')) {
                result = 9;
            } else if (incomingStrings.contains('♦')) {
                result = 22;
            } else if (incomingStrings.contains('♥')) {
                result = 35;
            } else {
                result = 48;
            }
        } else {
            person = incomingStrings[0];
            mast = incomingStrings[1];
        }

        if (mast == '♣') {  //0-12
            if (person.contains('A')) {
                    result = 0;
            } else if (person.contains('J')) {
                result = 10;
            } else if (person.contains('Q')) {
                result = 11;
            } else if (person.contains('K')) {
                result = 12;
            } else if ( (person.containsNone('A'))  && (person.containsNone('J')) && (person.containsNone('Q')) && (person.containsNone('K')) ) {
                Integer ifInteger = Integer.valueOf(person);
                result += ifInteger - 1;
            }
        } else if (mast=='♦') {
            if (person.contains('A')) {
                result = 13;
            } else if (person.contains('J')) {
                result = 23;
            } else if (person.contains('Q')) {
                result = 24;
            } else if (person.contains('K')) {
                result = 25;
            } else if ( (person.containsNone('A'))  && (person.containsNone('J')) && (person.containsNone('Q')) && (person.containsNone('K')) ) {
                Integer ifInteger = Integer.valueOf(person);
                result += 13;
                result += ifInteger - 1;
            }
        } else if (mast=='♥') {
            if (person.contains('A')) {
                result = 26;
            } else if (person.contains('J')) {
                result = 36;
            } else if (person.contains('Q')) {
                result = 37;
            } else if (person.contains('K')) {
                result = 38;
            } else if ( (person.containsNone('A'))  && (person.containsNone('J')) && (person.containsNone('Q')) && (person.containsNone('K')) ) {
                Integer ifInteger = Integer.valueOf(person);
                result += 26;
                result += ifInteger - 1;
            }
        } else if (mast=='♠') {
            if (person.contains('A')) {
                result = 39;
            } else if (person.contains('J')) {
                result = 49;
            } else if (person.contains('Q')) {
                result = 50;
            } else if (person.contains('K')) {
                result = 51;
            } else if ( (person.containsNone('A'))  && (person.containsNone('J')) && (person.containsNone('Q')) && (person.containsNone('K')) ) {
                Integer ifInteger = Integer.valueOf(person);
                result += 39;
                result += ifInteger - 1;
            }
        }
        return result;
    }
}