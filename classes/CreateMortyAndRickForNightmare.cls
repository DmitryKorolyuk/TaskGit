/**
 * Created by User on 27.02.2019.
 */

public with sharing class CreateMortyAndRickForNightmare {
    public void addSomeRecord() {
        List<Contact> mainListContacts = new List<Contact>();
        for(Integer i=0; i<4; i++) {
            if(Math.random() < 0.5) {
                Contact contactMorty = new Contact(LastName='Smith', FirstName='Morty');
                mainListContacts.add(contactMorty);
            } else {
                Contact contactRick = new Contact(LastName='Sanchez', FirstName='Rick');
                mainListContacts.add(contactRick);
            }
        } 
        insert mainListContacts;
    }
}