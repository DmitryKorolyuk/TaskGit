@isTest
public class Test_for_Trigger {
    @isTest
    public static void setup() {
        List<Storage__c> listStorage = new List<Storage__c>();
        
        Storage__c instance = new Storage__c(
            Name = 'TestName'
        );
        listStorage.add(instance);
        insert listStorage;
        
        TriggerHandler.methodForBeforeInsert(listStorage);
        System.assertEquals(false, [SELECT Id, Availability__c FROM Storage__c WHERE Id IN : listStorage][0].Availability__c);
        
        List<Storage__c> listStorage2 = new List<Storage__c>();
        Storage__c instance2 = new Storage__c(
            Name = 'TestName2',
            Amount__c = 1
        );
        listStorage2.add(instance2);
        insert listStorage2;
        
        TriggerHandler.methodForBeforeInsert(listStorage2);
        System.assertEquals(true, [SELECT Name, Availability__c FROM Storage__c WHERE Id IN : listStorage2][0].Availability__c);
        
        
        for(Storage__c item : listStorage) {
            item.Amount__c = 1;
        }
        update listStorage;
        
        TriggerHandler.methodForBeforeInsert(listStorage);
        System.assertEquals(true, [SELECT Id, Availability__c FROM Storage__c WHERE Id IN : listStorage][0].Availability__c);
        
        for(Storage__c item : listStorage) {
            item.Amount__c = 0;
        }
        update listStorage;
        TriggerHandler.methodForBeforeInsert(listStorage);
        System.assertEquals(false, [SELECT Id, Availability__c FROM Storage__c WHERE Id IN : listStorage][0].Availability__c);
    }
}