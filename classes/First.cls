public with sharing class First {
    public Contact con {get; set;}      // form for our new Contact.
    public String nameForDelete {get; set;}     // string for name, for delete.

    public First() {
        con = new Contact();        // reload form all time, when constructor restart.
    }

    public  void addContact() {     // insert contact? who we are type in input field.
        insert con;
    }

    public  void deleteContact() {      // delete contact from base, we have name contact which we need for delete
                                        // this name we put in input from VP and add in variable nameForDelete.
        delete [SELECT Id, LastName FROM  Contact WHERE LastName =: nameForDelete];
    }

}