public class LoopsTasks {

    /**
     * Returns the 'Fizz','Buzz' or an original number as String using the following rules:
     * 1) return original number as String
     * 2) but if number multiples of three return 'Fizz'
     * 3) for the multiples of five return 'Buzz'
     * 4) for numbers which are multiples of both three and five return 'FizzBuzz'
     *
     * @param {number} num
     * @return {any}
     *
     * @example
     *   2 =>  '2'
     *   3 => 'Fizz'
     *   5 => 'Buzz'
     *   4 => '4'
     *  15 => 'FizzBuzz'
     *  20 => 'Buzz'
     *  21 => 'Fizz'
     *
     */
    public static String getFizzBuzz(Integer num) {// ? !!
        String result = '';
        if (Math.mod(num, 3) == 0 && Math.mod(num, 5) == 0) {
            result = 'FizzBuzz';
        } else if (Math.mod(num, 3) == 0) {
            result = 'Fizz';
        } else if (Math.mod(num, 5) == 0) {
            result = 'Buzz';
        } else {
            result = String.valueOf(num);
        }
        return result;
    }

    /**
     * Returns the factorial of the specified integer n.
     *
     * @param {number} n
     * @return {number}
     *
     * @example:
     *   1  => 1
     *   5  => 120
     *   10 => 3628800
     */
    public static Integer getFactorial(Integer num) { //?? переписать в один цикл !!
        //List<Integer> listIntegers = new List<Integer>();
        /*
        for (Integer i = num; i > 0; i--) {
            listIntegers.add(i);
        }
        result = listIntegers[0];
        System.debug(listIntegers);
        */
        /*
        for (Integer i = 0; i < num; i++) {    // listIntegers.size()
            result += i * (i+1);     // listIntegers[i]
        }
        */
        /*
        Integer result = num;
        if (num==1) {
            result = num;
        } else {
            for (Integer i = num-1; i > 0; i--) {
                result *=  i  ;
            }
        }
        */
        Integer result = num;
        for (Integer i = num-1; i > 0; i--) {
            result *=  i  ;
        }
        System.debug(result);
        return result;
    }

    /**
     * Returns the sum of integer numbers between n1 and n2 (inclusive).
     *
     * @param {number} n1
     * @param {number} n2
     * @return {number}
     *
     * @example:
     *   1,2   =>  3  ( = 1+2 )
     *   5,10  =>  45 ( = 5+6+7+8+9+10 )
     *   -1,1  =>  0  ( = -1 + 0 + 1 )
     */
    public static Integer getSumBetweenNumbers(Integer num1, Integer num2) {
        Integer result = num1;
        List<Integer> listIntegers = new List<Integer>();
        for (Integer i = num1; i <= num2; i++) {
            listIntegers.add(i);
        }
        System.debug(listIntegers);
        for (Integer i = 1; i < listIntegers.size(); i++) {
            result = result + listIntegers[i];
        }
        return result;
    }

    /**
     * Returns true, if a triangle can be built with the specified sides a,b,c and false in any other ways.
     *
     * @param {number} a
     * @param {number} b
     * @param {number} c
     * @return {bool}
     *
     * @example:
     *   1,2,3    =>  false
     *   3,4,5    =>  true
     *   10,1,1   =>  false
     *   10,10,10 =>  true
     */
    public static Boolean isTriangle(Integer a, Integer b, Integer c) {//? a2+b2=c2  !!
        Boolean result = false;
        if ( ((a * a) + (b * b)) == (c * c) ) {
            result = true;
        } else if ( a == b && b == c ) {
            result = true;
        }
        /*
        if (a == b && b == c) {
            result = true;
        } else if ((a == 3 && b == 4 && c == 5) || (a == 4 && b == 5 && c == 3) || (c == 4 && a == 5 && b == 3)) {
            result = true;
        } else {
            result = false;
        }
        */
        System.debug(result);
        return result;
    }

    /**
     * Returns true, if two specified axis-aligned rectangles overlap, otherwise false.
     * Each rectangle representing by Map<String, Integer>
     *  {
     *     top: 5,
     *     left: 5,
     *     width: 20,
     *     height: 10
     *  }
     *
     *  (5;5)
     *     -------------
     *     |           |
     *     |           |  height = 10
     *     -------------
     *        width=20
     *
     * NOTE: Please use canvas coordinate space (https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes#The_grid),
     * it differs from Cartesian coordinate system.
     *
     * @param {Map<String, Integer>} rect1
     * @param {Map<String, Integer>} rect2
     * @return {bool}
     *
     * @example:
     *   { top: 0, left: 0, width: 10, height: 10 },
     *   { top: 5, left: 5, width: 20, height: 20 }    =>  true
     *
     *   { top: 0, left: 0, width: 10, height: 10 },
     *   { top:20, left:20, width: 20, height: 20 }    =>  false
     *
     */
    public static Boolean doRectanglesOverlap(Map<String, Integer> rect1, Map<String, Integer> rect2) {//? !!
        /*
        List<Integer> valuesInR1 = new List<Integer>();
        valuesInR1 = rect1.values();
        System.debug(valuesInR1);

        List<Integer> valuesInR2 = new List<Integer>();
        valuesInR2 = rect2.values();
        System.debug(valuesInR2);
        */
        /*
        Double resultInDouble = Math.sqrt( Math.pow((Double.valueOf(String.valueOf(rect2.get('top'))) - Double.valueOf(String.valueOf(rect1.get('top')))), 2) + Math.pow((Double.valueOf(String.valueOf(rect2.get('left'))) - Double.valueOf(String.valueOf(rect1.get('left')))) ,2) );
        System.debug(resultInDouble);
        Boolean answer = false;
        Double lengthDiagonalFirstRectangle = Math.sqrt( Math.pow((Double.valueOf(String.valueOf(rect1.get('width')))), 2) + Math.pow((Double.valueOf(String.valueOf(rect1.get('height')))), 2) );
        System.debug(lengthDiagonalFirstRectangle);
        //Double lengthDiagonalSecondRectangle = Math.sqrt( Math.pow((Double.valueOf(String.valueOf(rect2.get('width')))), 2) + Math.pow((Double.valueOf(String.valueOf(rect2.get('height')))), 2) );
        //System.debug(lengthDiagonalSecondRectangle);
        */
        Boolean answer = false;
        if ( ( (rect1.get('top') + rect1.get('width')) > (rect2.get('top')) && (rect1.get('left') + rect1.get('height')) > (rect2.get('left')) )) {
            answer = true;
        }
        return answer;
    }

    /**
     * Returns true, if point lies inside the circle, otherwise false.
     * Circle is:
     *     Center Map<String, Double>: {
     *       x: 5,
     *       y: 5
     *     },
     *     Radius Integer: 20
     * Point is Map<String, Double>
     *  {
     *     x: 5,
     *     y: 5
     *  }
     *
     * @param {Map<String, Double>} center
     * @param {Integer} raduis
     * @param {Map<String, Double>} point
     * @return {bool}
     *
     * @example:
     *   center: { x:0, y:0 }, radius: 10, point: { x:0, y:0 }     => true
     *   center: { x:0, y:0 }, radius:10,  point: { x:10, y:10 }   => false
     *
     */
    public static Boolean isInsideCircle(Map<String, Double> center, Integer radius, Map<String, Double> point) {//? use map not list !!
        /*
        //List<String> fromMapCenter = new List<String>();
        Set<String> keysFormMap = new Set<String>();
        keysFormMap = center.keySet();
        List<Double> listDoubles = center.values(); // kord from center
        System.debug('kord from center' + listDoubles);

        List<String> fromMapCenterP = new List<String>();
        Set<String> keysFormMapp = new Set<String>();
        keysFormMapp = point.keySet();
        List<Double> listDoublesp = point.values(); // kord from point
        System.debug('kord from point' + listDoublesp);
        */
        /*
        String integerInString = String.valueOf(radius);
        Double radiusInDouble = Double.valueOf(integerInString);
        */
        Double radiusInDouble = Double.valueOf(String.valueOf(radius) );
        /*
        Double resultDouble = Math.pow(listDoublesp[0] - listDoubles[0], 2) + Math.pow(listDoublesp[1] - listDoubles[1], 2);
        Double lengthBeePoint_Center = Math.sqrt(resultDouble);
        */
        Double lengthBeePoint_Center = Math.sqrt( Math.pow( (point.get('x') - center.get('x')) , 2) + Math.pow( (point.get('y') - center.get('y')) , 2) );
        Boolean answer = false;
        if ((lengthBeePoint_Center < radiusInDouble)) {
            answer = true;
        }
        return answer;
    }

    /**
     * Returns the first non repeated char in the specified strings otherwise returns null.
     *
     * @param {string} str
     * @return {string}
     *
     * @example:
     *   'The quick brown fox jumps over the lazy dog' => 'T'
     *   'abracadabra'  => 'c'
     *   'entente' => null
     */
    public static String findFirstSingleChar(String str) { //убрать переменные и не использовать лист (строку) !!
        /*
        List<String> splitStrings = str.split('');  // splitStrings - list string
        String result = null;
        for (Integer i = 0; i < splitStrings.size(); i++) {
            String innerStringsForRepeat = '';    //string for repeat
            for (Integer j = 1; j < splitStrings.size(); j++) {
                if (splitStrings[i] == splitStrings[j]) {
                    innerStringsForRepeat = innerStringsForRepeat + splitStrings[i];     // repeat put in string
                }
            }
            if (innerStringsForRepeat.length() == 1) {  //if length equals 1, 1 - letter in string - our target
                result = innerStringsForRepeat;   // put the liter in result string
                break;    // and break , mission finish
            }
        }
        */
        String toFinding =  str.substring(0, 1);
        String withoutFirstChar = str.substring(1);
        String result = null;
        if (withoutFirstChar.containsNone(toFinding)) {
            result = toFinding;
        } else {
            for (Integer i = 0; i < withoutFirstChar.length(); i++) {
                toFinding = str.substring(i, i+1);
                withoutFirstChar = str.substring(i+1);
                if (withoutFirstChar.containsNone(toFinding)) {
                    result = toFinding;
                    break;
                } else {
                    continue;
                }
            }
        }
        return result;
    }

    /**
     * Returns the string representation of math interval, specified by two points and include / exclude flags.
     * See the details: https://en.wikipedia.org/wiki/Interval_(mathematics)
     *
     * Please take attention, that the smaller number should be the first in the notation
     *
     * @param {number} a
     * @param {number} b
     * @param {bool} isStartIncluded
     * @param {bool} isEndIncluded
     * @return {string}
     *
     * @example
     *   0, 1, true, true   => '[0, 1]'
     *   0, 1, true, false  => '[0, 1)'
     *   0, 1, false, true  => '(0, 1]'
     *   0, 1, false, false => '(0, 1)'
     * Smaller number has to be first :
     *   5, 3, true, true   => '[3, 5]'
     *
     */
    public static String getIntervalString(Integer a, Integer b, Boolean isStartIncluded, Boolean isEndIncluded) {
        String main = '';
        if (isStartIncluded == true) {
            main = '[';    //  '\'' + '['
        } else {
            main = '(';
        }
        System.debug(main);
        if (a > b) {
            main = main + String.valueOf(b) + ', ' + String.valueOf(a);
        } else {
            main = main + String.valueOf(a) + ', ' + String.valueOf(b);
        }
        System.debug(main);
        if (isEndIncluded == true) {
            main = main + ']';   // ']' + '\''
        } else {
            main = main + ')';
        }
        System.debug(main);
        return main;
    }

    /**
     * Reverse the specified string (put all chars in reverse order)
     *
     * @param {string} str
     * @return {string}
     *
     * @example:
     * 'The quick brown fox jumps over the lazy dog' => 'god yzal eht revo spmuj xof nworb kciuq ehT'
     * 'abracadabra' => 'arbadacarba'
     * 'rotator' => 'rotator'
     * 'noon' => 'noon'
     */
    public static String reverseString(String str) { // ?? на реверс !!
        /*
        List<String> splitStrings = str.split('');
        List<String> resultStrings = new List<String>();
        String result = '';
        for (Integer i = splitStrings.size() - 1; i >= 0; i--) {
            resultStrings.add(splitStrings[i]);
        }
        System.debug(resultStrings);

        for (Integer i = 0; i < resultStrings.size(); i++) {
            result = result + resultStrings[i];
        }
        System.debug(result);
        */
        String result = str.reverse();
        return result;
    }

    /**
     * Reverse the specified integer number (put all digits in reverse order)
     *
     * @param {number} num
     * @return {number}
     *
     * @example:
     *   12345 => 54321
     *   1111  => 1111
     *   87354 => 45378
     *   34143 => 34143
     */
    public static Integer reverseInteger(Integer num) {
        String result = String.valueOf(num);
        String reversString = result.reverse();
        Integer resultInteger = Integer.valueOf(reversString);
        return resultInteger;
    }

    /**
     * Returns the digital root of integer:
     *   step1 : find sum of all digits
     *   step2 : if sum > 9 then goto step1 otherwise return the sum
     *
     * @param {number} n
     * @return {number}
     *
     * @example:
     *   12345 ( 1+2+3+4+5 = 15, 1+5 = 6) => 6
     *   23456 ( 2+3+4+5+6 = 20, 2+0 = 2) => 2
     *   10000 ( 1+0+0+0+0 = 1 ) => 1
     *   165536 (1+6+5+5+3+6 = 26,  2+6 = 8) => 8
     */
    public static Integer getDigitalRoot(Integer num) {
        String stringInputIntegers = String.valueOf(num);
        List<String> splitStrings = stringInputIntegers.split('');
        System.debug(splitStrings);
        List<Integer> listForSum = new List<Integer>();
        for (Integer i = 0; i < splitStrings.size(); i++) {
            listForSum.add(Integer.valueOf(splitStrings[i])) ;  //list integers, from list stings
        }
        System.debug(listForSum);
        Integer firstSum = 0;
        for (Integer i = 0; i < listForSum.size(); i++) {
            firstSum = firstSum + listForSum[i];
        }
        System.debug('firstSum ' + firstSum);   //first sum
        String forAfterSumInteger = String.valueOf(firstSum);
        List<String> splitStringsAfterFirstSum = forAfterSumInteger.split('');
        Integer resultSum = 0;
        List<Integer> resultListIntegers = new List<Integer>();
        for (Integer i = 0; i < splitStringsAfterFirstSum.size(); i++) {
            resultListIntegers.add(Integer.valueOf(splitStringsAfterFirstSum[i])) ;  //list integers, from list stings
        }
        if (resultListIntegers.size() == 1) {
            resultSum = resultListIntegers[0];
        } else {
            for (Integer i = 0; i < resultListIntegers.size(); i++) {
                resultSum = resultSum + resultListIntegers[i];
            }
        }
        System.debug('result mla ' + resultSum);
        return resultSum;
    }

    /**
     * Returns true if the specified string has the balanced brackets and false otherwise.
     * Balanced means that is, whether it consists entirely of pairs of opening/closing brackets
     * (in that order), none of which mis-nest.
     * Brackets include [],(),{},<>
     *
     * @param {string} str
     * @return {boolean}
     *
     * @example:
     *   '' => true
     *   '[]'  => true
     *   '{}'  => true
     *   '()   => true
     *   '[[]' => false
     *   ']['  => false
     *   '[[][][[]]]' => true
     *   '[[][]][' => false
     *   '{)' = false
     *   '{[(<{[]}>)]}' = true
     */
    public static Boolean isBracketsBalanced(String str) {
        System.debug('str' + str);
        Integer incomingStringLength = str.length();    // length incoming string
        Integer counter = 0;  // for open Brackets
        Integer counterClose = 0;  // for open Brackets
        Boolean answer = true;
        List<String> listIncoming = str.split('');    //array split incoming string

        String reverseIncoming = str.reverse();
        System.debug('reverseIncoming' + reverseIncoming);
        List<String> listReversString = reverseIncoming.split('');

        if (incomingStringLength == 0) {
            answer = true;
        } else if (Math.mod(incomingStringLength, 2) != 0) {
            answer = false;
        } else if (listIncoming[0].contains('}')) {
            answer = false;
        } else if (listIncoming[0].contains(')')) {
            answer = false;
        } else if (listIncoming[0].contains(']')) {
            answer = false;
        } else if (listIncoming[0].contains('>')) {
            answer = false;
        } else {
            for (Integer i = 0; i < listIncoming.size(); i++) {
                if (listIncoming[i].contains('{')) {
                    counter++;
                } else if (listIncoming[i].contains('(')) {
                    counter++;
                } else if (listIncoming[i].contains('[')) {
                    counter++;
                } else if (listIncoming[i].contains('<')) {
                    counter++;
                } else {
                    counterClose++;
                }
            }
            if (counter == counterClose && (counter == 1)) {
                if ((listIncoming[0].contains('{')) != (listIncoming[1].contains('}'))) {
                    answer = false;
                } else if ((listIncoming[0].contains('(')) != (listIncoming[1].contains(')'))) {
                    answer = false;
                } else if ((listIncoming[0].contains('[')) != (listIncoming[1].contains(']'))) { //
                    answer = false;
                } else if ((listIncoming[0].contains('<')) != (listIncoming[1].contains('>'))) {
                    answer = false;
                }
            } else if ((counter == counterClose) && (counter == 2)) {
                if ((listIncoming[0].contains('{')) != (listReversString[0].contains('}'))) {
                    answer = false;
                } else if ((listIncoming[0].contains('(')) != (listReversString[0].contains(')'))) {
                    answer = false;
                } else if ((listIncoming[0].contains('[')) != (listReversString[0].contains(']'))) {
                    answer = false;
                } else if ((listIncoming[0].contains('<')) != (listReversString[0].contains('>'))) {
                    answer = false;
                }
            } else if ((counter == counterClose) && (counter > 2)) {
                Integer halfArray = listIncoming.size() / 2;
                for (Integer i = 0; i < listIncoming.size(); i++) {
                    if ((listIncoming[i].contains('{')) != (listReversString[i].contains('}'))) {
                        answer = false;
                    } else if ((listIncoming[i].contains('(')) != (listReversString[i].contains(')'))) {
                        answer = false;
                    } else if ((listIncoming[i].contains('[')) != (listReversString[i].contains(']'))) {
                        answer = false;
                    } else if ((listIncoming[i].contains('<')) != (listReversString[i].contains('>'))) {
                        answer = false;
                    } else if ((listIncoming[i].contains('{')) == (listReversString[i].contains('}'))) {
                        answer = true;
                    } else if ((listIncoming[i].contains('(')) == (listReversString[i].contains(')'))) {
                        answer = true;
                    } else if ((listIncoming[i].contains('[')) == (listReversString[i].contains(']'))) {
                        answer = true;
                    } else if ((listIncoming[i].contains('<')) == (listReversString[i].contains('>'))) {
                        answer = true;
                    }
                }
                if ((listIncoming[halfArray].contains('(')) != (listIncoming[halfArray + 1].contains(')'))) {
                    answer = false;
                }
            }
        }
        return answer;
    }

    /**
     * Returns the human readable string of time period specified by the start and end time.
     * The result string should be constrcuted using the folliwing rules:
     *
     * ---------------------------------------------------------------------
     *   Difference                 |  Result
     * ---------------------------------------------------------------------
     *    0 to 45 seconds           |  a few seconds ago
     *   45 to 90 seconds           |  a minute ago
     *   90 seconds to 45 minutes   |  2 minutes ago ... 45 minutes ago
     *   45 to 90 minutes           |  an hour ago
     *  90 minutes to 22 hours      |  2 hours ago ... 22 hours ago
     *  22 to 36 hours              |  a day ago
     *  36 hours to 25 days         |  2 days ago ... 25 days ago
     *  25 to 45 days               |  a month ago
     *  45 to 345 days              |  2 months ago ... 11 months ago
     *  345 to 545 days (1.5 years) |  a year ago
     *  546 days+                   |  2 years ago ... 20 years ago
     * ---------------------------------------------------------------------
     *
     * @param {DateTime} startDate
     * @param {DateTime} endDate
     * @return {string}
     *
     * @example
     *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:01')  => 'a few seconds ago'
     *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:05')  => '5 minutes ago'
     *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-02 03:00:05')  => 'a day ago'
     *   DateTime('2000-01-01 01:00:00'), DateTime('2015-01-02 03:00:05')  => '15 years ago'
     *
     */
    public static String timespanToHumanString(DateTime startDate, DateTime endDate) {
        String main = '';
        List<String> listTimes = new List<String>{
                'seconds', 'minute', 'minutes', 'hour', 'hours', 'day', 'days', 'month', 'months', 'year', 'years'
        };
        String startA = 'a ';
        String startAn = 'an ';
        String finish = ' ago';
        Long timeStart = startDate.getTime();
        Long timeEnd = endDate.getTime();
        System.debug('timeStart = ' + timeStart);
        System.debug('timeEnd = ' + timeEnd);
        System.debug('resultSeparate = ' + (timeEnd - timeStart) / 1000 + '_______________________');
        if ((timeEnd - timeStart) / 1000 >= 0 && (timeEnd - timeStart) / 1000 <= 45) {
            main = 'a few seconds ago';
        } else if ((timeEnd - timeStart) / 1000 > 45 && (timeEnd - timeStart) / 1000 <= 90) {
            main = 'a minute ago';
        } else if ((timeEnd - timeStart) / 1000 > 90 && (timeEnd - timeStart) / 1000 <= (45 * 60)) {
            Long howManyMinutes = (timeEnd - timeStart) / 1000;
            System.debug('====' + howManyMinutes);
            Decimal resultInDecimal = Decimal.valueOf(howManyMinutes);
            Decimal result = (resultInDecimal / 60).round(System.RoundingMode.HALF_DOWN);
            System.debug('=+ ' + resultInDecimal / 60);
            System.debug('=+ /' + result);
            System.debug('round = ' + result);
            main = result + ' minutes ago';
        } else if ((timeEnd - timeStart) / 1000 > 45 * 60 && (timeEnd - timeStart) / 1000 <= 90 * 60) {
            main = 'an hour ago';
        } else if ((timeEnd - timeStart) / 1000 > 90 * 60 && (timeEnd - timeStart) / 1000 <= 22 * 3600) {
            Long howManyHours = (timeEnd - timeStart) / 1000;
            Decimal resultDecimalHour = Decimal.valueOf(howManyHours);
            Decimal resultHour = (resultDecimalHour / 3600).round(System.RoundingMode.HALF_DOWN);
            main = resultHour + ' hours ago';
        } else if ((timeEnd - timeStart) / 1000 > 22 * 3600 && (timeEnd - timeStart) / 1000 <= 36 * 3600) {
            main = 'a day ago';
        } else if ((timeEnd - timeStart) / 1000 > 36 * 3600 && (timeEnd - timeStart) / 1000 <= 25 * 3600 * 24) {
            Long howManyDays = (timeEnd - timeStart) / 1000;
            Decimal resultDecimalDays = Decimal.valueOf(howManyDays);
            Decimal resultDays = (resultDecimalDays / 86400).round(System.RoundingMode.HALF_DOWN);
            main = resultDays + ' days ago';
        } else if ((timeEnd - timeStart) / 1000 > 25 * 3600 * 24 && (timeEnd - timeStart) / 1000 <= 45 * 3600 * 24) {
            main = 'a month ago';
        } else if ((timeEnd - timeStart) / 1000 > 45 * 3600 * 24 && (timeEnd - timeStart) / 1000 <= 345 * 3600 * 24) {
            Long howManyMonths = (timeEnd - timeStart) / 1000;
            Decimal resultDecimalMonths = Decimal.valueOf(howManyMonths);
            Decimal resultMoths = (resultDecimalMonths / 2592000).round(System.RoundingMode.HALF_DOWN);
            main = resultMoths + ' months ago';
        } else if ((timeEnd - timeStart) / 1000 > 345 * 3600 * 24 && (timeEnd - timeStart) / 1000 <= 545 * 3600 * 24) {
            main = 'a year ago';
        } else if ((timeEnd - timeStart) / 1000 > 545 * 3600 * 24) {
            Long howManyYears = (timeEnd - timeStart) / 1000;
            Decimal resultDecimalYeas = Decimal.valueOf(howManyYears);
            Decimal resultYears = (resultDecimalYeas / 31104000).round(System.RoundingMode.HALF_DOWN);
            main = resultYears + ' years ago';
        }
        System.debug('resultMAin = ' + main);
        return main;
    }
}