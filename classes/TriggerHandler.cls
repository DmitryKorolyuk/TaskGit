public class TriggerHandler {
    public static void methodForBeforeInsert(List<Storage__c> listForInsert) {
        for(Storage__c item : listForInsert) {
            if(item.Amount__c > 0) {
                item.Availability__c = true;
            } else {
                item.Availability__c = false;
            }
        }
    }
}