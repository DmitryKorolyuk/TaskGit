public with sharing class NumberTasks {

    /**
 * Returns an area of a rectangle given by width and heigth.
 *
 * @param {numder} width
 * @param {number} height
 * @return {number}
 *
 * @example:
 *   5, 10 => 50
 *   5, 5  => 25
 */
    public static Integer getRectangleArea(Integer width, Integer height) {
        Integer result=width*height;
        return result;
    }

    /**
 * Returns a circumference of circle given by radius.
 *
 * @param {number} radius
 * @return {number}
 *
 * @example:
 *   5    => 31.41592653589793
 *   3.14 => 19.729201864543903
 *   0    => 0
 */
    public static Decimal getCicleCircumference(Decimal radius) {
        Decimal result = 2*Math.PI*radius;
        return result;
    }

    /**
 * Returns an average of two given numbers.
 *
 * @param {numder} value1
 * @param {number} value2
 * @return {number}
 *
 * @example:
 *   5, 5  => 5
 *  10, 0  => 5
 *  -3, 3  => 0
 */
    public static Double getAverage(Double value1, Double value2) {
        Double result;
        if (value1==value2) {
            result = value1;
        } else {
            result = (value1+value2)/2;
        }
        return result;
    }

    /**
 * Returns a distance beetween two points by cartesian coordinates.
 *
 * @param {number} x1
 * @param {number} y1
 * @param {number} x2
 * @param {number} y2
 *
 * @return {number}
 *
 * @example:
 *   (0,0) (0,1)    => 1
 *   (0,0) (1,0)    => 1
 *   (-5,0) (10,-10) => 18.027756377319946
 */
    public static Decimal getDistanceBetweenPoints(Integer x1, Integer y1, Integer x2, Integer y2) {

        //math.abs(value)
        //myInt.format() - int in string
        //Integer resultInInteger =
        /*
        String x1S = String.valueOf(x1);
        Double x1D = Double.valueOf(x1S);
        String x2S = String.valueOf(x2);
        Double x2D = Double.valueOf(x2S);
        String y1S = String.valueOf(y1);
        Double y1D = Double.valueOf(y1S);
        String y2S = String.valueOf(y2);
        Double y2D = Double.valueOf(y2S);
        //Double resultDouble = Math.sqrt(  Math.pow( (math.abs(x2D)-math.abs(x1D) ), 2 ) + Math.sqrt(  Math.pow( (math.abs(y2D)-math.abs(y1D) ), 2 ))  );

                Double resultDouble = Math.sqrt(  Math.pow( x2D-x1D, 2 ) + Math.sqrt(  Math.pow( y2D-y1D, 2 )))  ;

                System.debug('result in double = ' + resultDouble);
        */
        String x1S = String.valueOf(x1);
        Double x1D = Double.valueOf(x1S);
        String x2S = String.valueOf(x2);
        Double x2D = Double.valueOf(x2S);
        String y1S = String.valueOf(y1);
        Double y1D = Double.valueOf(y1S);
        String y2S = String.valueOf(y2);
        Double y2D = Double.valueOf(y2S); // for math pow
        /*
        Decimal xO = Decimal.intValue(x1);
        Decimal xT = Decimal.intValue(x2);
        Decimal yO = Decimal.intValue(y1);
        Decimal yT = Decimal.intValue(y2);
        */
        Double resultDouble =  Math.pow( x2D-x1D, 2 ) +   Math.pow( y2D-y1D, 2 );
        Decimal res = Decimal.valueOf(Math.sqrt(resultDouble)) ;
        System.debug(res);
        //Decimal resultInDecimal = Decimal.valueOf(resultDouble);
        return res;
    }

    /**
 * Returns a root of linear equation a*x + b = 0 given by coefficients a and b.
 *
 * @param {number} a
 * @param {number} b
 * @return {number}
 *
 * @example:
 *   5*x - 10 = 0    => 2
 *   x + 8 = 0       => -8
 *   5*x = 0         => 0
 */
    public static Integer getLinearEquationRoot(Integer a, Integer b) {
        Integer result = (-1*b)/a;
        return result;
    }

    /**
 * Returns an angle (in radians) between two vectors given by xi and yi, coordinates in Cartesian plane
 * See details https://en.wikipedia.org/wiki/Euclidean_vector#Representations
 *
 * @param {number} x1
 * @param {number} y1
 * @param {number} x2
 * @param {number} y2
 * @return {number}
 *
 * @example:
 *   (1,0) (0,1)     => π/2
 *   (0,1) (0,-1)    => π
 *   (0,-1) (1,0)    => π/2
 *   (0,1) (0,1)     => 0
 *   (0,1) (1,2)     => 0
 */
    public static Decimal getAngleBetweenVectors(Integer x1, Integer y1, Integer x2, Integer y2) {
        System.debug(x1 + ' ' + y1 + ' ' + x2 + ' ' + y2 );
        String aStr = String.valueOf(x1);
        String bStr = String.valueOf(x2);
        String cStr = String.valueOf(y1);
        String yStr = String.valueOf(y2);
        Decimal aD = Decimal.valueOf(aStr);
        Decimal bD = Decimal.valueOf(bStr);
        Decimal cD = Decimal.valueOf(cStr);
        Decimal yD = Decimal.valueOf(yStr);
        Decimal result = (aD*bD) + (cD*yD);
        System.debug('sum ' + result);
        Decimal res =0;
        if ( result==0) {
            res = Math.PI/2;
        } else if ( result < 0 ) {
            res = Math.PI;
        }
        return res;
    }

    /**
 * Returns a number by given string representation.
 *
 * @param {string} value
 * @return {number}
 *
 * @example:
 *    '100'     => 100
 *     '37'     => 37
 * '-525.5'     => -525.5
 */
    public static Double parseNumberFromString(String value) {
        Double result = Double.valueOf(value);
        return result;
    }

    /**
 * Returns a diagonal length of the rectangular parallelepiped given by its sides a,b,c.
 *
 * @param {number} a
 * @param {number} b
 * @param {number} c
 * @return {number}
 *
 * @example:
 *   1,1,1   => 1.7320508075688772
 *   3,3,3   => 5.196152422706632
 *   1,2,3   => 3.741657386773941
 */
    public static Decimal getParallelipidedDiagonal(Integer a, Integer b, Integer c) {//sqrt(a2+b2+c2)
        String aStr = String.valueOf(a);
        String bStr = String.valueOf(b);
        String cStr = String.valueOf(c);
        Decimal aD = Decimal.valueOf(aStr);
        Decimal bD = Decimal.valueOf(bStr);
        Decimal cD = Decimal.valueOf(cStr);
        Decimal result = Math.sqrt((aD*aD)+(bD*bD)+(cD*cD));
        return result;
    }

    /**
 * Returns the number rounded to specified power of 10.
 *
 * @param {number} num
 * @param {number} pow
 * @return {number}
 *
 * @example:
 *   1234, 0  => 1234
 *   1234, 1  => 1230
 *   1234, 2  => 1200
 *   1234, 3  => 1000
 *   1678, 0  => 1678
 *   1678, 1  => 1680
 *   1678, 2  => 1700
 *   1678, 3  => 2000
 */
    public static Double roundToPowerOfTen(Double num, Double pow) {
        Integer intResult =0;
        Double result=0;
        String resultString = '';
        if (pow == 1) {
            Double oneD = num / 10;
            Long one = oneD.round();
            one = one *10;
            System.debug(one);
            intResult = one.intValue();
            System.debug(intResult);
            resultString = String.valueOf(intResult);
            result = Double.valueOf(resultString);
        } else if (pow == 2) {
            Double twoD = num / 100;
            Long two = twoD.round();
            two = two * 100;
            System.debug(two);

            intResult = two.intValue();
            resultString = String.valueOf(intResult);
            result = Double.valueOf(resultString);
        } else if (pow == 3) {
            Double threeD = num / 1000;
            Long three = threeD.round();
            three = three *1000;
            System.debug(three);

            intResult = three.intValue();
            resultString = String.valueOf(intResult);
            result = Double.valueOf(resultString);
        } else {
            result = num;

        }
        return result;
    }
}