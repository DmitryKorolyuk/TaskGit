public with sharing class CreateClassForSection4 {

    public void createCarsAndEngine() {
        List<Engine__c> listEngines = new List<Engine__c>();
        for (Integer i=0; i<100; i++) {
            Engine__c newEngine = new Engine__c(Name='engine'+i);
            listEngines.add(newEngine);
        }
        insert listEngines;

        List<Car__c> listCar = new List<Car__c>();
        List<Detail__c> listDetails = new List<Detail__c>();

        for (Integer j=0; j<100; j++) {
            Engine__c engineVariable = listEngines[j];

            for (Integer i=0; i<2; i++) {
                Car__c newCar = new Car__c(Name='car'+i +j, Engine__c=engineVariable.Id);
                listCar.add(newCar);
            }
            for (Integer k=0; k<2; k++) {
                Detail__c newDetail = new Detail__c(Name='detail'+k +j , Engine__c=engineVariable.Id);
                listDetails.add(newDetail);
            }
        }
        insert listCar;
        insert listDetails;
    }

    public List<Engine__c> returnListEngines() {	//in list all engines
        List<Engine__c> listEnginesForReturn = [SELECT Id, Name FROM Engine__c];
        System.debug('listEnginesForReturn ' + listEnginesForReturn);
        System.debug('listEnginesForReturn size = ' + listEnginesForReturn.size());
      return  listEnginesForReturn; 
    }

    public List<List<Car__c>> returnListCar() {		// in lists, all car for each engine

        List<Engine__c> listEngines = [SELECT Id, Name FROM Engine__c];
        List<Car__c> cars = [SELECT Id, Name, Engine__c FROM Car__c WHERE Engine__c  =: listEngines];
        System.debug('cars size = ' + cars.size());

        List<List<Car__c>> listCarsForEachEngine = new List<List<Car__c>>();
        
        for (Integer i=0; i<listEngines.size(); i++) {
            List<Car__c> innerlist = new List<Car__c>() ;
            for (Integer j=0; j<cars.size(); j++) {
                if(cars[j].Engine__c == listEngines[i].Id) {
                    innerlist.add(cars[j]);
                }
            }    
            listCarsForEachEngine.add(innerlist); 
        }
        System.debug('listCarsForEachEngine ' + listCarsForEachEngine);
        System.debug('listCarsForEachEngine size = ' + listCarsForEachEngine.size());
    return  listCarsForEachEngine;
    }

    public Map<Id, List<String>> returnDetailsByIdCar(){	//map with key - Id car and value - all details for car // hz?

        List<Engine__c> listEnginesReturnFromBase = [SELECT Id, (SELECT Id, Name FROM Cars__r), (SELECT Id, Name FROM Details__r) FROM Engine__c];

        Map<Id, List<String>> mapReturn = new Map<Id, List<String>>();
        for (Engine__c eng: listEnginesReturnFromBase) {

            List<String> listDetail = new List<String>();
            if (!eng.Details__r.isEmpty()) {
                for (Detail__c det: eng.Details__r) {
                    listDetail.add(det.Name);
                }
            }
            if (!eng.Cars__r.isEmpty()) {
                for (Car__c car: eng.Cars__r) {
                    mapReturn.put(car.Id, new List<String>());
                    mapReturn.get(car.Id).addAll(listDetail);
                }
            }
        }
        System.debug(mapReturn);
        return null;
    }

    public Map<String, List<String>> returnCarsByIdEngine() {		//map with key - name engine and value - all cars for this engine //

        Map<String, List<String>> mapR = new Map<String, List<String>>();

        List<Engine__c> listEnginesFromDatabase = [SELECT Id, Name, (SELECT Id, Name FROM Cars__r) FROM Engine__c];
        System.debug(listEnginesFromDatabase);

        for (Engine__c eng: listEnginesFromDatabase) {
            if (!eng.Cars__r.isEmpty()) {
                mapR.put(eng.Name, new List<String>()); // fill name engine to map key
                for (Car__c car: eng.Cars__r) {
                    mapR.get(eng.Name).add(car.Name);   // get by Id and add in list<String> value(car name)
                }
            }
        }
        System.debug(mapR);
        return mapR;
    }
	
    public void insertToBaseClone() {
        List<Engine__c> listEnginesFromBase = [SELECT Id, OwnerId, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastViewedDate, LastReferencedDate FROM Engine__c];
        List<Engine__c> cloneListEngine = new List<Engine__c>();
        for (Engine__c item : listEnginesFromBase) {
            Engine__c clonedEngine = item.clone(false, false, false, false);
            cloneListEngine.add(clonedEngine);
        }
        insert cloneListEngine;
        
        List<Detail__c> listDetailsFromBase = [SELECT Id, OwnerId, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastViewedDate, LastReferencedDate, Engine__c FROM Detail__c];
        List<Detail__c> cloneListDetail = new List<Detail__c>();
        for (Detail__c item : listDetailsFromBase) {
            Detail__c clonedDetail = item.clone(false, false, false, false);
            cloneListDetail.add(clonedDetail);
        }
        insert cloneListDetail;
        
		List<Car__c> listCarsFromBase = [SELECT Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedById, LastModifiedDate, SystemModstamp, LastViewedDate, LastReferencedDate, Engine__c FROM Car__c];
        List<Car__c> cloneListCar = new List<Car__c>();
        for (Car__c item : listCarsFromBase) {
            Car__c clonedCar = item.clone(false, false, false, false);
            cloneListCar.add(clonedCar);
        }
        insert cloneListCar;
    }

    public void deleteRecords(){

        List<Detail__c> listDetailsFromBase = [SELECT Id, Name  FROM Detail__c];
        Map<String, Id> mapForFindingDetail = new Map<String, Id>();
        for (Detail__c item : listDetailsFromBase) {
            mapForFindingDetail.put(item.Name, item.Id);
        }
        List<Id> listIdDetails = new List<Id>();
        for (String key : mapForFindingDetail.keySet()) {
            listIdDetails.add(mapForFindingDetail.get(key));
        }
        delete [SELECT Id FROM Detail__c WHERE Id NOT IN :listIdDetails];
 /*      
        List<Detail__c> listDetailToDelete = new List<Detail__c>();
		List<Detail__c> listDetailsFromBase = [SELECT Id, Name  FROM Detail__c];
		Set<String> setString = new Set<String>();
        for(Detail__c detail : listDetailsFromBase) {
            if(setString.contains(detail.Name)) {
                listDetailToDelete.add(detail);
            } else {
                setString.add(detail.Name);
            }
        }
		delete listDetailToDelete;
 */      
        
        List<Engine__c> listEnginesFromBase = [SELECT Id, Name  FROM Engine__c];
        Map<String, Id> mapForFindingEngine = new Map<String, Id>();
        for (Engine__c item : listEnginesFromBase) {
            mapForFindingEngine.put(item.Name, item.Id);
        }
        List<Id> listIdEngines = new List<Id>();
        for (String key : mapForFindingEngine.keySet()) {
            listIdEngines.add(mapForFindingEngine.get(key));
        }
        delete [SELECT Id FROM Engine__c WHERE Id NOT IN :listIdEngines];


        List<Car__c> listCarsFromBase = [SELECT Id, Name  FROM Car__c];
        Map<String, Id> mapForFindingCar = new Map<String, Id>();
        for (Car__c item : listCarsFromBase) {
            mapForFindingCar.put(item.Name, item.Id);
        }
        List<Id> listIdCars = new List<Id>();
        for (String key : mapForFindingCar.keySet()) {
            listIdCars.add(mapForFindingCar.get(key));
        }
        delete [SELECT Id FROM Car__c WHERE Id NOT IN :listIdCars];

    }
}