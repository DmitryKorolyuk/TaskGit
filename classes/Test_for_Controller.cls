@isTest
public class Test_for_Controller {
    @isTest
    public static void setup() {
        Controller_for_Page instance = new Controller_for_Page();
        Storage__c instanceStorage = new Storage__c(
        	Name = 'test',
            Amount__c = 0
        );
        insert instanceStorage;
        Integer countProductInBaseBefore = [SELECT COUNT() 
                               				FROM Storage__c];
        instance.IdForEdit = instanceStorage.Id;
        
        instanceStorage.Amount__c = 1;
        update instanceStorage;
        
        instance.storage = [SELECT Id, Name, Cost__c, Amount__c, Type__c, Date_Added__c, Date_Release__c, Availability__c FROM Storage__c WHERE Id =: instance.IdForEdit];
        instance.showEditPopup();
        
        instance.editProduct();
        instance.SortTable();
        
        System.assertEquals(1, [SELECT Id, Amount__c FROM Storage__c WHERE Id =: instanceStorage.Id][0].Amount__c);
        System.assertEquals(countProductInBaseBefore, [SELECT COUNT() FROM Storage__c]);
    }
    
    @isTest
    public static void getProductFromBase() { //getProductFromBase();
        Controller_for_Page instance = new Controller_for_Page();
        instance.stringWithInputFromPage = 't';
        instance.getProductFromBase();
        
        instance.dateForSearchStart = null;
        instance.dateForSearchEnd = Date.today();
        instance.stringWithInputFromPage = '';
        instance.getProductFromBase();
        
        instance.dateForSearchStart = Date.today();
        instance.dateForSearchEnd = null;
        instance.stringWithInputFromPage = '';
        instance.getProductFromBase();
        
        instance.dateForSearchStart = Date.today();
        instance.dateForSearchEnd = Date.today();
        instance.stringWithInputFromPage = '';
        instance.getProductFromBase();
        
        instance.dateForSearchStart = Date.today();
        instance.dateForSearchEnd = Date.today();
        instance.stringWithInputFromPage = 't';
        instance.getProductFromBase();
    }
        
    @isTest
    public static void getTotalPages() {
        Controller_for_Page instance = new Controller_for_Page();
        instance.list_size = 2;
        instance.total_records = 10;
        instance.getTotalPages();
        
        System.assertEquals(5, instance.getTotalPages());
        
        instance.total_records = 11;
        instance.getTotalPages();
        
        System.assertEquals(6, instance.getTotalPages());
    }
    
    @isTest
    public static void Next() {
        Controller_for_Page instance = new Controller_for_Page();
        instance.pages = 1;
        instance.counter = 0;
        instance.Next();
        
        System.assertEquals(2, instance.pages);
    }
    
    @isTest
    public static void Previous() {
        Controller_for_Page instance = new Controller_for_Page();
        instance.pages = 1;
        instance.counter = 0;
        instance.Next();
        instance.Previous();
        
        System.assertEquals(1, instance.pages);
    }
    
    @isTest
    public static void getDisablePrevious() {
        Controller_for_Page instance = new Controller_for_Page();
        instance.pages = 1;
        instance.counter = 0;
        
        System.assertEquals(true, instance.getDisablePrevious());
    }
    
    @isTest
    public static void getDisableNext() {
        Controller_for_Page instance = new Controller_for_Page();
        instance.pages = 0;
        
        System.assertEquals(true, instance.getDisableNext());
    }
        
    @isTest
    public static void addNewProduct() {
        Integer countBefore = [SELECT COUNT() 
                               FROM Storage__c];
        
        Storage__c storage = new Storage__c(
        	Name = 'testAdd',
            Amount__c = 0 
        );
        insert storage;
        Controller_for_Page instance = new Controller_for_Page();
        instance.addProduct();
        instance.showAddPopup();
        
        System.assertNotEquals(countBefore, [SELECT COUNT() 
                                             FROM Storage__c]);
    }
    
    @isTest
    public static void deleteProduct() {
        Storage__c instanceStorage = new Storage__c(
        	Name = 'test',
            Amount__c = 1
        );
        insert instanceStorage;
        Integer countProductBefore = [SELECT COUNT() FROM Storage__c];
        
        Controller_for_Page instance = new Controller_for_Page();
        instance.showDelPopup();
        instance.IdForDel = instanceStorage.Id;
        instance.StringForDel = 'test';
        instance.deleteProduct();
        instance.closeDelPopup();
        
        System.assertNotEquals(countProductBefore, [SELECT COUNT() FROM Storage__c]);
    }
}